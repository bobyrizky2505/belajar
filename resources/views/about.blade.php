<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/style.css">
    <title>BelajarLaravel | About</title>
</head>
<body>
    <h1>Ini Halaman About</h1>
    <h3>{{ $nama }}</h3>
    <h3>{{ $email }}</h3>
    <img src="{{ $image }}" alt="{{ $nama }}" style="width: 100px;height:auto;">
</body>
</html>